// ----------------------------------------------------------------------------
// contains the handler functions for user management
// ----------------------------------------------------------------------------

package main

import (
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

// handles user login
func loginHandler(w http.ResponseWriter, r *http.Request) {

	// checks username and password, returning a cookie if correct
	username := r.FormValue("userName")
	password := r.FormValue("password")

	cookie, passwordCorrect := p.RequestCookie(username, password)
	if passwordCorrect == nil {
		http.SetCookie(w, &cookie)
		pageData.LoggedIn = true
		pageData.UserID = cookie.Value
		pageData.UserName = username
		pageData.Pools = getUserPools(pageData.UserID)
		pageData.MotiveColls = getUserMotiveColls(pageData.UserID)
		pageData.Msg = "Willkommen, " + username
	} else {
		pageData.LoggedIn = false
		pageData.UserID = ""
		pageData.UserName = ""
		pageData.Msg = "Benutzername oder Passwort falsch!"
	}

	// execute the mainHandler with new pageData
	mainHandler(w, r)
}

// handles logout
func logoutHandler(w http.ResponseWriter, r *http.Request) {

	// logging out the user by setting LoggedIn to false and
	// deleting the cookie by setting its MaxAge < 0
	pageData.LoggedIn = false
	pageData.UserName = ""
	pageData.UserID = ""
	pageData.Msg = "Auf Wiedersehen!"
	pageData.CurrentPage = pageData.Pages["LandingPage"]

	if len(r.Cookies()) > 0 {
		cookie := r.Cookies()[0]
		cookie.MaxAge = -1
		http.SetCookie(w, cookie)
	}

	// execute the mainHandler with new pageData
	mainHandler(w, r)
}

// handles displaying the register new user page
func registerHandler(w http.ResponseWriter, r *http.Request) {

	// Sets the Register flag and executes the mainHandler
	pageData.Register = true

	mainHandler(w, r)
}

// handles getting back to login screen from register
func toLoginHandler(w http.ResponseWriter, r *http.Request) {

	// Unsets register flag and Msg
	pageData.Register = false
	pageData.Msg = ""

	mainHandler(w, r)
}

// handles the registration of a new User
func registerUser(w http.ResponseWriter, r *http.Request) {

	// read username and password from form. add user or return
	// error messages.
	userName := r.FormValue("userName")
	pw1 := r.FormValue("password")
	pw2 := r.FormValue("passwordRepeat")

	if pw1 != pw2 {
		pageData.Msg = "Passwörter stimmen nicht überein."
		pageData.Register = true
		pageData.LoggedIn = false
	} else if pw1 == "" || userName == "" {
		pageData.Msg = "Name oder Passwort dürfen nicht leer sein."
		pageData.Register = true
		pageData.LoggedIn = false
	} else {
		err := p.AddUser(userName, pw1)
		if err != nil {
			pageData.Msg = err.Error()
			pageData.Register = true
			pageData.LoggedIn = false
		} else {
			pageData.Msg = "Neuer Benutzer registriert"
			pageData.Register = false
			pageData.LoggedIn = false
			userID := p.GetUserID(userName)
			db.C(userCollection).Insert(user{ID: bson.ObjectIdHex(userID)})
		}
	}

	mainHandler(w, r)
}
