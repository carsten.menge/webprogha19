// ----------------------------------------------------------------------------
// mosaicHandling.go contains all logic pertaining to creating mosaics
// ----------------------------------------------------------------------------
package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"math"
	"math/rand"
	"sort"

	"github.com/disintegration/imaging"
	"gopkg.in/mgo.v2/bson"
)

// function to make a new mosaic from motivePic using pool using one of the n
// best fitting candidates
func makeMosaic(name string, pool pool, motivePic motivePic, n int) {

	// --------------------------------------------------------------------------
	// create our gridfs file
	filename := name + ".jpg"
	newMosaic, err := gridfs.Create(filename)
	if err != nil {
		errorHandler(err)
	}
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// add to collection
	mosaic := mosaicPic{}
	mosaic.ID = bson.NewObjectId()
	mosaic.Name = name
	mosaic.Done = false
	mosaic.UserID = pool.UserID
	mosaic.FileID = newMosaic.Id().(bson.ObjectId)
	db.C(mosaicCollection).Insert(mosaic)
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// add to the user's mosaics
	push := bson.M{"$push": bson.M{"MosaicsIDs": mosaic.ID}}
	db.C(userCollection).UpdateId(pool.UserID, push)
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// get the baseimage from gridfs
	gridFile, err := gridfs.OpenId(motivePic.FileID)
	if err != nil {
		errorHandler(err)
	}
	defer gridFile.Close()
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// decode the base image
	baseImg, err := imaging.Decode(gridFile, imaging.AutoOrientation(true))
	if err != nil {
		errorHandler(err)
	}
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// prepare a variable for the tiles
	var tileImg image.Image
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// create the empty destination image
	bounds := baseImg.Bounds()
	dstImgBounds := image.Rect(0, 0, bounds.Max.X*pool.Size, bounds.Max.Y*pool.Size)
	dstImg := image.NewNRGBA(dstImgBounds)
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// run through the pixels of the base image
	for y := bounds.Min.Y; y <= bounds.Max.Y; y++ {
		for x := bounds.Min.X; x <= bounds.Max.X; x++ {

			// get a fitting Pool image
			poolPic := getFittingPoolPic(pool, baseImg.At(x, y), n)
			gridFile, err := gridfs.OpenId(poolPic.FileID)
			if err != nil {
				errorHandler(err)
			}
			defer gridFile.Close()

			// decode the tile image
			tileImg, err = imaging.Decode(gridFile, imaging.AutoOrientation(true))
			if err != nil {
				errorHandler(err)
			}

			// rectangle where the tile image is supposed to go as determined by
			// the pixels on the base image multiplied by the pool's size in px
			r := image.Rect(
				x*pool.Size,
				y*pool.Size,
				(x*pool.Size)+pool.Size,
				(y*pool.Size)+pool.Size,
			)

			// draw the tile into the destination image
			draw.Draw(
				dstImg,
				r,
				tileImg,
				tileImg.Bounds().Min,
				draw.Src,
			)
			fmt.Printf("Drawing %v, %v\n", x, y)
		}
	}
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// encode the destination image as JPEG
	imaging.Encode(newMosaic, dstImg, imaging.JPEG)
	err = newMosaic.Close()
	if err != nil {
		errorHandler(err)
	}
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// update mosaic in collection when we're done
	mosaic.Done = true
	db.C(mosaicCollection).UpdateId(mosaic.ID, mosaic)
	// --------------------------------------------------------------------------
}

// function returning a fitting pool image
func getFittingPoolPic(pool pool, col color.Color, n int) poolPic {

	// --------------------------------------------------------------------------
	// prepare a slice to hold the candidates in a pair of pic and color distance
	var candidates []poolPicDistPair
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// run through the pool pictures and get the color distance
	for _, poolPicID := range pool.PicsIDs {

		poolPic := getPoolPicByID(poolPicID)

		poolPicAvgCol := color.RGBA{
			uint8(poolPic.MetaData.AvgColor.R),
			uint8(poolPic.MetaData.AvgColor.G),
			uint8(poolPic.MetaData.AvgColor.B),
			uint8(poolPic.MetaData.AvgColor.A),
		}
		candidates = append(candidates,
			poolPicDistPair{
				poolPic,
				getColorDistance(poolPicAvgCol, col),
			})
	}
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// sort by distance, throw away the least fitting and select one at random
	sort.Sort(byColorDistance(candidates))
	candidates = candidates[0:n]
	selected := candidates[rand.Intn(len(candidates))]
	return selected.poolPic
	// --------------------------------------------------------------------------
}

// function to return the distance between two colors A and B
func getColorDistance(colorA, colorB color.Color) float64 {

	aR, aB, aG, _ := colorA.RGBA()
	bR, bB, bG, _ := colorB.RGBA()

	dR := aR - bR
	dB := aB - bB
	dG := aG - bG

	d := math.Sqrt(
		math.Pow(float64(dR), 2) +
			math.Pow(float64(dB), 2) +
			math.Pow(float64(dG), 2))

	return d
}

// implement sorting interface for sorting by color distance
type poolPicDistPair struct {
	poolPic poolPic
	colDist float64
}

type byColorDistance []poolPicDistPair

func (c byColorDistance) Len() int {
	return len(c)
}

func (c byColorDistance) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

func (c byColorDistance) Less(i, j int) bool {
	return c[i].colDist < c[j].colDist
}
