package pfoertner

import (
	"errors"

	"gopkg.in/mgo.v2/bson"
)

// AddUser with userName and password. Returns an error if the user already exists.
func (s *Session) AddUser(userName string, password string) error {

	if !s.userNameExists(userName) {
		user := User{
			userName,
			password,
		}
		s.userCollection.Insert(user)
		return nil
	}

	return errors.New("user already exists in this database")
}

// RemoveUser with userName. Returns error, if the user does not exist.
func (s *Session) RemoveUser(userName string) error {

	if s.userNameExists(userName) {
		s.userCollection.Remove(bson.M{"UserName": userName})
		return nil
	}

	return errors.New("user does not exist in this database")
}

// userNameExists checks wether a user with a given name string already exists
func (s *Session) userNameExists(userName string) bool {

	count, _ := s.userCollection.Find(bson.M{"UserName": userName}).Count()
	if count > 0 {
		return true
	}
	return false
}

// GetUserID returns the user's id
func (s *Session) GetUserID(userName string) string {

	user := UserRead{}
	s.userCollection.Find(bson.M{"UserName": userName}).One(&user)
	return user.ID.Hex()
}
