package pfoertner

import (
	"log"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// TODO: make constants user manageable instead
const (
	collName   = "pfoertnerUserCollection"
	expiryTime = 48
)

// Session object
type Session struct {
	mgoSession     *mgo.Session
	db             *mgo.Database
	userCollection *mgo.Collection
}

// User object
type User struct {
	UserName string `bson:"UserName"`
	Password string `bson:"Password"`
}

// UserRead user read object, contains ID
type UserRead struct {
	ID       bson.ObjectId `bson:"_id" json:"ID"`
	UserName string        `bson:"UserName"`
	Password string        `bson:"Password"`
}

type cookieRequest struct {
	UserName string
	Password string
}

// cookieResponse object. Contains:
//		UserName 	string
//		IDstring 	string
//		Expires  	time.Time
// This object is used to generate the login cookie
type cookieResponse struct {
	UserName string
	IDstring string
	Expires  time.Time
}

// New creates a new instance of pfoertner using a database address.
//		dbAddress is the address for the MongoDB server
//		dbName is the desired name for the DB
func New(dbAddress, dbName string) *Session {

	mgoSession, err := mgo.Dial(dbAddress)
	if err != nil {
		log.Fatal(err)
	}

	db := mgoSession.DB(dbName)

	userCollection := db.C(collName)

	s := Session{
		mgoSession,
		db,
		userCollection,
	}

	return &s
}
